<?php

namespace app\controllers;

use app\models\Loan;
use app\models\Payments;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;



class SiteController extends Controller
{

    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $Loan = new Loan();
        if(Yii::$app->request->isPost && $Loan->load(Yii::$app->request->post())){
            $Loan->save();
            Payments::CalculatePayments($Loan);
            return $this->redirect(Url::toRoute(['site/payments', 'id' => $Loan->id]));
        }
        return $this->render('index', ['model' => $Loan]);
    }

    public function actionLoans(){
        $data_provider = new ActiveDataProvider([
           'query' => Loan::find()
        ]);
        return $this->render('loan', ['data_provider' => $data_provider]);
    }

    public function actionPayments($id){
        $data_provider = new ActiveDataProvider([
            'query' => Payments::find()
                ->where(['loan_id' => $id])
                ->orderBy(['payment_number' => SORT_ASC])
        ]);
        return $this->render('payments', ['data_provider' => $data_provider]);
    }

}
