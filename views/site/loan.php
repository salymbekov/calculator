<?php

/* @var $this yii\web\View */
/* @var $data_provider ActiveDataProvider */

$this->title = 'Таблица займов';


use yii\data\ActiveDataProvider;
use yii\grid\GridView; ?>

<div class="site-index">
    <h2 class="text-center">Таблица займов</h2>
    <hr>
    <div class="row">
        <?= GridView::widget([
            'dataProvider' => $data_provider,
            'columns' => [
                'start_date',
                'loan_amount',
                'loan_duration',
                'annual_interest_rate',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{payments}',
                    'buttons' => [
                        'payments' => function ($url, $model) {
                            return \yii\helpers\Html::a('График платежей', \yii\helpers\Url::toRoute(['site/payments', 'id' => $model->id]), ['class' => 'btn btn-default']);
                        }
                    ]
                ],
            ]
        ]); ?>
    </div>

</div>
