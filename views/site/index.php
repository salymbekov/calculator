<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;

?>
<div class="site-index">
    <h2 class="text-center">Калькулятор графика расчета аннуитетных платежей</h2>
    <hr>
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'start_date')->widget(DatePicker::class, [
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control']
        ]) ?>


        <?= $form->field($model, 'loan_amount')->textInput()?>

        <?= $form->field($model, 'loan_duration')->textInput() ?>

        <?= $form->field($model, 'annual_interest_rate')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
