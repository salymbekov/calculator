<?php

/* @var $this yii\web\View */
/* @var $data_provider ActiveDataProvider */

$this->title = 'Таблица платежей';


use yii\data\ActiveDataProvider;
use yii\grid\GridView; ?>

<div class="site-index">
    <h2 class="text-center">Таблица платежей</h2>
    <hr>
    <div class="row">
        <?= GridView::widget([
            'dataProvider' => $data_provider,
            'columns' => [
                'payment_number',
                'payment_date',
                'payment_amount',
                'interest_amount',
                'main_sum_amount',
                'remaining_loan'
            ]
        ]); ?>
    </div>

</div>
