<?php

use yii\db\Migration;

/**
 * Class m201110_140416_init
 */
class m201110_140416_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('loans', [
            'id' => $this->primaryKey(),
            'start_date' => $this->date(),
            'loan_amount' => $this->integer(),
            'loan_duration' => $this->integer(),
            'annual_interest_rate' => $this->integer()

        ]);

        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'loan_id' => $this->integer(),
            'payment_number' => $this->integer(),
            'payment_date' => $this->date(),
            'payment_amount' => $this->integer(),
            'interest_amount' => $this->integer(),
            'main_sum_amount' => $this->integer(),
            'remaining_loan' => $this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('loans');
        $this->dropTable('payments');
    }

}
