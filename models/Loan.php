<?php


namespace app\models;

use yii\db\ActiveRecord;


/**
 * Loan model
 *
 * @property integer $id
 * @property integer $loan_duration
 * @property string $start_date
 * @property integer $loan_amount
 * @property integer $annual_interest_rate
 *
 */
class Loan extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_date', 'loan_duration', 'loan_amount', 'annual_interest_rate'], 'required', 'message'=>'Поле <{attribute}> не может быть пустым.'],
            [['start_date', 'loan_duration', 'loan_amount', 'annual_interest_rate'], 'filter', 'filter' => 'trim'],
            [['loan_duration', 'loan_amount', 'annual_interest_rate'], 'integer', 'message'=>'Поле <{attribute}> должно быть числом.'],
            [['start_date'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_date' => 'Начальная дата',
            'loan_duration' => 'Срок займа (в месяцах)',
            'loan_amount' => 'Сумма займа',
            'annual_interest_rate' => 'Годовая процентная ставка'
        ];
    }

}