<?php


namespace app\models;


use yii\db\ActiveRecord;

/**
 * Payments model
 *
 * @property integer $id
 * @property integer $loan_id
 * @property integer $payment_number
 * @property string $payment_date
 * @property integer $payment_amount
 * @property integer $interest_amount
 * @property integer $main_sum_amount
 * @property integer $remaining_loan
 *
 */

class Payments extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['loan_id', 'payment_number', 'payment_date', 'payment_amount', 'interest_amount', 'main_sum_amount', 'remaining_loan'], 'required', 'message'=>'Поле <{attribute}> не может быть пустым.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_number' => 'Номер платежа',
            'payment_date' => 'Дата платежа',
            'payment_amount' => 'Общая сумма платежа',
            'interest_amount' => 'Сумма погашаемых процентов',
            'main_sum_amount' => 'Сумма погашаемого основного долга',
            'remaining_loan' => 'Остаток основного долга'
        ];
    }

    public static function CalculatePayments(Loan $Loan)
    {
        $monthly_interest_rate = $Loan->annual_interest_rate / 100 / 12;
        $payment_amount = $Loan->loan_amount * ($monthly_interest_rate + $monthly_interest_rate / ((1 + $monthly_interest_rate) ** $Loan->loan_duration - 1));
        $remaining_loan = $Loan->loan_amount;
        for ($i = 1; $i <= $Loan->loan_duration; $i++) {
            $interest_amount = $remaining_loan * $monthly_interest_rate;
            $main_payment = $payment_amount - $interest_amount;
            $remaining_loan -= $main_payment;
            $payment_date = date('Y-m-d', strtotime("+$i months", strtotime($Loan->start_date)));
            $Payments = new Payments();
            $Payments->payment_amount = round($payment_amount);
            $Payments->interest_amount =  round($interest_amount);
            $Payments->main_sum_amount =  round($main_payment);
            $Payments->remaining_loan =  round($remaining_loan);
            $Payments->payment_date =  $payment_date;
            $Payments->payment_number = $i;
            $Payments->loan_id = $Loan->id;
            $Payments->save();
        }
    }
}